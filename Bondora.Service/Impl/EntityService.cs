﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bondora.Data.Domain;
using Bondora.Data.Repos.Interfaces;
using Bondora.Data.UnitOfWork.Interfaces;
using Bondora.Service.Interfaces;

namespace Bondora.Service.Impl
{
    public abstract class EntityService<T> : IEntityService<T> where T : BaseEntity
    {
        IUnitOfWork _unitOfWork;
        IRepository<T> _repository;

        protected EntityService(IUnitOfWork unitOfWork, IRepository<T> repository)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
        }


        public virtual void Create(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            _repository.Add(entity);
        }


        public virtual void Update(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _repository.Edit(entity);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public virtual void AddOrUpdate(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");

            if (entity.IsNew)
            {
                _repository.Add(entity);
            }
            else
            {
                _repository.Edit(entity);
            }
        }

        public virtual void Delete(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _repository.Remove(entity);
        }

        public virtual IQueryable<T> GetAll()
        {
            return _repository.GetAll();
        }
    }
}
