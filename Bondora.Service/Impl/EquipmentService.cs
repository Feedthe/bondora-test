﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using AutoMapper;
using Bondora.Data.Cache;
using Bondora.Data.Domain;
using Bondora.Data.Repos.Interfaces;
using Bondora.Data.UnitOfWork.Interfaces;
using Bondora.Data.ViewModels.Home;
using Bondora.Service.Interfaces;

namespace Bondora.Service.Impl
{
    public class EquipmentService : EntityService<Equipment>, IEquipmentService
    {
        private IMapper MapperConfig;

        public EquipmentService(IUnitOfWork unitOfWork, IRepository<Equipment> repository, IMapper mapperConfig) : base(unitOfWork, repository)
        {
            MapperConfig = mapperConfig;
        }

        public IEnumerable<Rental> MakeRentals(IEnumerable<EquipmentViewModel> model)
        {
            var rentedItems = model.Where(x => x.Days > 0);
            IEnumerable<Rental> rentals = MapperConfig.Map<IEnumerable<EquipmentViewModel>, IEnumerable<Rental>>(rentedItems).ToList();

            return rentals;
        }
    }
}