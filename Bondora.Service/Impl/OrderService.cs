﻿using System.Collections.Generic;
using System.Text;
using Bondora.Data.Cache;
using Bondora.Data.Domain;
using Bondora.Data.Repos.Interfaces;
using Bondora.Data.UnitOfWork.Interfaces;
using Bondora.Service.Interfaces;

namespace Bondora.Service.Impl
{
    public class OrderService : EntityService<Order>, IOrderService
    {
        private IOrderRepository OrderRepository;
        private IUnitOfWork UnitOfWork;

        public OrderService(IUnitOfWork unitOfWork, IOrderRepository repository) : base(unitOfWork, repository)
        {
            OrderRepository = repository;
            UnitOfWork = unitOfWork;
        }

        public Order MakeOrder(IEnumerable<Rental> rentals)
        {
            var order = new Order(rentals);
            OrderRepository.Add(order);
            UnitOfWork.Commit();
            return order;
        }

        public Order GetById(int id)
        {
            return OrderRepository.Get(id);
        }

        public string GenerateTextInvoice(int id)
        {
            var key = "OrderService." + nameof(GenerateTextInvoice) + id;

            string filestring;

            if (CacheHelper.Get(key, out filestring)) return filestring;

            var order = GetById(id);
            if (order == null)
            {
                return filestring;
            }

            var sb = new StringBuilder();
            sb.AppendLine($"Summary of order #{id}\r\n");

            foreach (var rental in order.Rentals)
            {
                sb.AppendLine($"{rental.Item.Name}\t\t\t {rental.CalculatePrice()}");
            }

            sb.AppendLine("\r\n");
            sb.AppendLine($"Total price: {order.OrderTotal} € \t\t Total bonus: {order.BonusTotal}");

            filestring = sb.ToString();

            CacheHelper.Add(filestring, key);

            return filestring;
        }
    }
}
