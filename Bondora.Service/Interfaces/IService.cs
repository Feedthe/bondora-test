﻿using System.Collections.Generic;

namespace Bondora.Service.Interfaces
{
    public interface IService
    {
        void Commit();
    }
}
