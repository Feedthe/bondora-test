﻿using System.Collections.Generic;
using Bondora.Data.Domain;
using Bondora.Data.ViewModels.Home;

namespace Bondora.Service.Interfaces
{
    public interface IEquipmentService : IEntityService<Equipment>
    {
        IEnumerable<Rental> MakeRentals(IEnumerable<EquipmentViewModel> model);
    }
}