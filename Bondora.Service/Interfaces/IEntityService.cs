﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bondora.Service.Interfaces
{
    public interface IEntityService<T> : IService
    {
        void Create(T entity);
        void Delete(T entity);
        IQueryable<T> GetAll();
        void Update(T entity);
        void AddOrUpdate(T entity);
    }
}
