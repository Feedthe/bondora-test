﻿using System.Collections.Generic;
using Bondora.Data.Domain;

namespace Bondora.Service.Interfaces
{
    public interface IOrderService : IEntityService<Order>
    {
        Order MakeOrder(IEnumerable<Rental> model);
        Order GetById(int id);
        string GenerateTextInvoice(int id);
    }
}
