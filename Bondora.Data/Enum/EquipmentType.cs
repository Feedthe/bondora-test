﻿namespace Bondora.Data.Enum
{
    public enum EquipmentType
    {
        Heavy = 0,
        Regular = 1,
        Specialized = 2
    }
}
