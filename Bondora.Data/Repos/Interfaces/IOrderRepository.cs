﻿using Bondora.Data.Domain;

namespace Bondora.Data.Repos.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {

    }
}
