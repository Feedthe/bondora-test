﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bondora.Data.Domain;

namespace Bondora.Data.Repos.Interfaces
{
    public interface IRepositoryWithTypedId<T, TId> : IRepository, IQueryable<T> where T : BaseEntity
    {
        T Get(TId id);

        void Add(T entity);

        void Remove(T entity);

        void Remove(TId id);

        void Commit();

        void SaveOrUpdate(T entity);

        void Edit(T entity);

        IQueryable<T> GetAll(); 
    }
}
