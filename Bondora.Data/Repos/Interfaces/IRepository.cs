﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bondora.Data.Domain;

namespace Bondora.Data.Repos.Interfaces
{
    public interface IRepository
    {
    }

    public interface IRepository<T> : IRepositoryWithTypedId<T, long> where T : BaseEntity
    {

    }
}
