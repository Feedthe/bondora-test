﻿using System.Data.Entity;
using Bondora.Data.Domain;
using Bondora.Data.Repos.Interfaces;

namespace Bondora.Data.Repos.Impl
{
    public class Repository<T> : RepositoryWithTypedId<T, long>, IRepository<T> where T : BaseEntity
    {
        public Repository(DbContext context) : base(context)
        {
        }
    }
}
