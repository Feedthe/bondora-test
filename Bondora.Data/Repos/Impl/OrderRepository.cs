﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using Bondora.Data.Cache;
using Bondora.Data.Domain;
using Bondora.Data.Repos.Interfaces;

namespace Bondora.Data.Repos.Impl
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {

        public OrderRepository(DbContext context) : base(context)
        {
        }
    }
}
