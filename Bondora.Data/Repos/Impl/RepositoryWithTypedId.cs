﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using Bondora.Data.Domain;
using Bondora.Data.Repos.Interfaces;

namespace Bondora.Data.Repos.Impl
{
    public abstract class RepositoryWithTypedId<T, TId> : IRepositoryWithTypedId<T, TId> where T : BaseEntity
    {
        protected DbContext personalContext;
        protected readonly IDbSet<T> dbset;

        protected RepositoryWithTypedId(DbContext context)
        {
            personalContext = context;
            dbset = context.Set<T>();
        }


        public IEnumerator<T> GetEnumerator()
        {
            return dbset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Expression Expression => dbset.Expression;

        public Type ElementType => dbset.ElementType;

        public IQueryProvider Provider => dbset.Provider;

        public virtual T Get(TId id)
        {
            return dbset.Find(id);
        }

        public virtual void Add(T entity)
        {
            dbset.Add(entity);
        }

        public virtual void Edit(T entity)
        {
            personalContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Remove(T entity)
        {
            dbset.Remove(entity);
        }

        public virtual void Remove(TId id)
        {
            var entity = Get(id);
            Remove(entity);
        }

        public virtual void Commit()
        {
            personalContext.SaveChanges();
        }

        public virtual void SaveOrUpdate(T entity)
        {
            if (Equals(entity.Id, default(TId)))
                Add(entity);
            Commit();
        }

        public IQueryable<T> GetAll()
        {
            return dbset;
        }
    }
}
