﻿using System.Collections.Generic;
using System.Linq;

namespace Bondora.Data.Domain
{
    public class Order : BaseEntity
    {
        public virtual ICollection<Rental> Rentals { get; set; }

        public Order()
        {
            Rentals = new List<Rental>();
        }

        public Order(IEnumerable<Rental> rentals)
        {
            Rentals = rentals.ToList();
        }

        public decimal OrderTotal
        {
            get { return Rentals.Sum(rental => rental.CalculatePrice()); }
        }

        public long BonusTotal
        {
            get { return Rentals.Sum(rental => rental.CalculateBonus()); }
        }
    }
}
