﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bondora.Data.Interfaces;

namespace Bondora.Data.Domain
{
    [Serializable]
    public abstract class BaseEntityWithTypedId<T> : IEntity<T>
    {
        public T Id { get; set; }

        public bool IsNew => Equals(Id, default(T));
    }
}
