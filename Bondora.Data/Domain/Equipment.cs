﻿using System.Collections.Generic;
using Bondora.Data.Enum;

namespace Bondora.Data.Domain
{
    public class Equipment : BaseEntity
    {
        public Equipment()
        { 
            //default values
            OneTimeFee = Config.Config.OneTimeFee;
            PremiumFee = Config.Config.PremiumFee;
            RegularFee = Config.Config.RegularFee;
        }

        public Equipment(string name, EquipmentType type) : this()
        {
            Name = name;
            EquipmentType = type;
            if (type == EquipmentType.Regular)
            {
                PremiumDays = Config.Config.RegularPremiumDays;
            }
            if (type == EquipmentType.Specialized)
            {
                PremiumDays = Config.Config.SpecializedPremiumDays;
            }
            BonusRate = type.Equals(EquipmentType.Heavy) ? Config.Config.HeavyBonusRate : Config.Config.DefaultBonusRate;
        }

        public string Name { get; set; }


        //rates are stored per item to keep the result correct all the time 
        //
        //if rates are stored in one place and used for every item, then changing 
        //the rates would result in incorrect price calculation for old orders).
        //Same for bonus.
        public decimal OneTimeFee { get; set; }
        public decimal PremiumFee { get; set; }
        public decimal RegularFee { get; set; }

        public int PremiumDays { get; set; }

        public int BonusRate { get; set; }

        public EquipmentType EquipmentType { get; set; }
        public ICollection<Rental> Rentals { get; set; }

        public decimal CalculatePrice(int days)
        {
            if (EquipmentType == EquipmentType.Heavy)
            {
                return OneTimeFee + PremiumFee * days;
            }
            if (EquipmentType == EquipmentType.Regular)
            {
                return OneTimeFee + SlidingFee(PremiumDays, days);
            }
            if (EquipmentType == EquipmentType.Specialized)
            {
                return SlidingFee(PremiumDays, days);
            }
            return 0m;
        }

        private decimal SlidingFee(int premiumFeeDays, int days)
        {
            if (days <= premiumFeeDays)
            {
                return PremiumFee * days;
            }

            var baseFee = PremiumFee * premiumFeeDays;

            var regularFeeDays = days - premiumFeeDays;

            return baseFee + (RegularFee * regularFeeDays);
        }
    }
}
