﻿namespace Bondora.Data.Domain
{
    public class Rental : BaseEntity
    {
        public Rental(Equipment item, int days)
        {
            Item = item;
            Days = days;
        }

        public Rental()
        {
        }

        public virtual Equipment Item { get; set; }
        public int ItemId { get; set; }
        public int Days { get; set; }
        public virtual Order Order { get; set; }
        public int OrderId { get; set; }

        public decimal CalculatePrice()
        {
            return Item.CalculatePrice(Days);
        }

        public int CalculateBonus()
        {
            return Item.BonusRate;
        }
    }
}
