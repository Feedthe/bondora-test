﻿using System;

namespace Bondora.Data.UnitOfWork.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        int Commit();
    }
}
