﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bondora.Data.UnitOfWork.Interfaces;

namespace Bondora.Data.UnitOfWork.Impl
{
    public sealed class UnitOfWork : IUnitOfWork
    {

        private DbContext dbContext;

       
        public UnitOfWork(DbContext context)
        {
            dbContext = context;
        }

        public int Commit()
        {
            return dbContext.SaveChanges();
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing) return;
            if (dbContext == null) return;
            dbContext.Dispose();
            dbContext = null;
        }
    }
}
