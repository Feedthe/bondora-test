﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bondora.Data.Config
{
    public static class Config
    {
        public static decimal OneTimeFee = 100;
        public static decimal PremiumFee = 60;
        public static decimal RegularFee = 40;
        public static int DefaultBonusRate = 1;
        public static int HeavyBonusRate = 2;
        public static int RegularPremiumDays  = 2;
        public static int SpecializedPremiumDays = 3;

    }
}
