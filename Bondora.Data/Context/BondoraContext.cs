﻿using System;
using System.Data.Entity;
using System.Linq;
using Bondora.Data.Domain;
using Bondora.Data.Interfaces;

namespace Bondora.Data.Context
{
    public class BondoraContext : DbContext
    {
        public BondoraContext() : base("Name=BondoraTest")
        {
            
        }

        public object CurrentUser { get; set; }
        public object CurrentUserId { get; set; }

        public DbSet<Rental> Rentals { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Equipment> Equipment { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Rental>().HasRequired(x => x.Item).WithMany(x => x.Rentals).HasForeignKey(x => x.ItemId);
            modelBuilder.Entity<Rental>().HasRequired(x => x.Order).WithMany(x => x.Rentals).HasForeignKey(x => x.OrderId);
            base.OnModelCreating(modelBuilder);
        }

    }

}
