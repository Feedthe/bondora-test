﻿using System.Data.Entity;
using Bondora.Data.Domain;
using Bondora.Data.Enum;

namespace Bondora.Data.Context
{
    public class BondoraInitializer : DropCreateDatabaseIfModelChanges<BondoraContext>
    {
        protected override void Seed(BondoraContext context)
        {
            context.Configuration.AutoDetectChangesEnabled = true;

            AddEquipment(context);

            context.SaveChanges();
        }

        private void AddEquipment(BondoraContext context)
        {
            var items = new[]
            {
                new Equipment("Caterpillar truck", EquipmentType.Heavy),
                new Equipment("KamAZ truck", EquipmentType.Regular),
                new Equipment("Komatsu crane", EquipmentType.Heavy),
                new Equipment("Volvo steamroller", EquipmentType.Regular),
                new Equipment("Bosch jackhammer", EquipmentType.Specialized)
            };

            foreach (var item in items)
            {
                context.Equipment.Add(item);
            }
        }
    }
}
