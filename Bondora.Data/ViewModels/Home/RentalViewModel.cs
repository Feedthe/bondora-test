﻿namespace Bondora.Data.ViewModels.Home
{
    public class RentalViewModel
    {
        public EquipmentViewModel Item { get; set; }
        public int Days { get; set; }
        public decimal TotalPrice { get; set; }
        public int TotalBonus { get; set; }
    }
}