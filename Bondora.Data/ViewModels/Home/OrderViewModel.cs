﻿using System.Collections.Generic;
using System.Linq;

namespace Bondora.Data.ViewModels.Home
{
    public class OrderViewModel
    {
        public int Id { get; set; }

        public IEnumerable<RentalViewModel> Rentals { get; set; }

        public decimal TotalOrderPrice
        {
            get { return Rentals.Sum(x => x.TotalPrice); }
        }

        public object TotalOrderBonus
        {
            get { return Rentals.Sum(x => x.TotalBonus); }
        }

    }
}