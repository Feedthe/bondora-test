﻿using System.ComponentModel.DataAnnotations;
using Bondora.Data.Enum;

namespace Bondora.Data.ViewModels.Home
{
    public class EquipmentViewModel
    {
        [Required]
        public int Id { get; set; }
        public string Name { get; set; }
        public EquipmentType EquipmentType { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public int Days { get; set; }
    }
}