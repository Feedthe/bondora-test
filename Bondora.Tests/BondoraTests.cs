﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bondora.Data.Domain;
using Bondora.Data.Enum;
using NUnit.Framework;

namespace Bondora.Tests
{
    [TestFixture]
    public class BondoraTests
    {

        [Test]
        public void TestHeavyPrice()
        {
            var heavy = new Equipment("Heavy", EquipmentType.Heavy);

            var days = 2;
            var rental = new Rental(heavy, days);

            var order = new Order(new List<Rental>{rental});

            Assert.AreEqual(100m + (60m * days), rental.CalculatePrice());
            Assert.AreEqual(100m + (60m * days), order.OrderTotal);

            var multipleItemOrder = new Order();

            for (var i = 1; i < 5; i++)
            {
                var r = new Rental(heavy, i);

                multipleItemOrder.Rentals.Add(r);
            }

            Assert.AreEqual((4 * 100m) + 60m + 120m + 180m + 240m, multipleItemOrder.OrderTotal);
        }

        [Test]
        public void TestRegularPrice()
        {
            var regular = new Equipment("Regular", EquipmentType.Regular);

            var premiumDays = 2;
            var regulardays = 3;
            var totaldays = premiumDays + regulardays;
            var rental = new Rental(regular, totaldays);

            var order = new Order(new List<Rental> { rental });

            Assert.AreEqual(100m + (60m * premiumDays) + (40m * regulardays), rental.CalculatePrice());
            Assert.AreEqual(100m + (60m * premiumDays) + (40m * regulardays), order.OrderTotal);

            var multipleItemOrder = new Order();

            for (var i = 1; i < 5; i++)
            {
                var r = new Rental(regular, i);

                multipleItemOrder.Rentals.Add(r);
            }

            Assert.AreEqual((4 * 100m) + 60m + 120m +(120m + 40m) + (120m + 80m), multipleItemOrder.OrderTotal);
        }
        [Test]
        public void TestSpecialPrice()
        {
            var special = new Equipment("Special", EquipmentType.Specialized);

            var premiumDays = 3;
            var regulardays = 3;
            var totaldays = premiumDays + regulardays;
            var rental = new Rental(special, totaldays);

            var singleItemOrder = new Order(new List<Rental> { rental });
            
            Assert.AreEqual((60m * premiumDays) + (40m * regulardays), rental.CalculatePrice());
            Assert.AreEqual((60m * premiumDays) + (40m * regulardays), singleItemOrder.OrderTotal);

            var multipleItemOrder = new Order();

            for (var i = 1; i < 5; i++)
            {
                var r = new Rental(special, i);

                multipleItemOrder.Rentals.Add(r); 
            }

            Assert.AreEqual(60m + 120m + 180m + (180m + 40m), multipleItemOrder.OrderTotal);
        }

        [Test]
        public void TestBonus()
        {
            var regular = new Equipment("Regular", EquipmentType.Regular);
            var heavy = new Equipment("Heavy", EquipmentType.Heavy);
            var special = new Equipment("Special", EquipmentType.Specialized);

            var multipleItemOrder = new Order();


            multipleItemOrder.Rentals.Add(new Rental(regular, 4));

            Assert.AreEqual(1, multipleItemOrder.BonusTotal);

            multipleItemOrder.Rentals.Add(new Rental(regular, 4));
            multipleItemOrder.Rentals.Add(new Rental(special, 4));

            Assert.AreEqual(3, multipleItemOrder.BonusTotal);

            multipleItemOrder.Rentals.Add(new Rental(heavy, 4));
            multipleItemOrder.Rentals.Add(new Rental(heavy, 4));

            Assert.AreEqual(7, multipleItemOrder.BonusTotal);

            ////////////

            var order = new Order();

            for (var i = 1; i <= 1000; i++)
            {
                order.Rentals.Add(new Rental(regular, i));
            }

            for (var i = 1; i <= 1000; i++)
            {
                order.Rentals.Add(new Rental(special, i));
            }

            for (var i = 1; i <= 1000; i++)
            {
                order.Rentals.Add(new Rental(heavy, i));
            }
            Assert.AreEqual(4000, order.BonusTotal);

        }
    }
}
