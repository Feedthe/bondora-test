# Bondora test assignment



### Prerequisites


```
MS Visual Studio
MSSQL SERVER/SQLEXPRESS
```


The solution is done using ASP.NET MVC and uses EF code first approach and, even though it was not mandatory, uses a database to store data. Database will be seeded with some data on first app run. 
A few third party libraries are also used - AutoMapper and Autofac 

### Installing

```
Open solution
Restore packages
Update DB connection string username and password in web.config in Bondora project (the user must have dbcreator role). 
You might also need to remove 'Integrated Security=SSPI' from the connection string, depending on your sql server config.
Run Bondora project
```


