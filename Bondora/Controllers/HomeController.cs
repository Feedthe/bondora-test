﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Bondora.Data.ViewModels.Home;
using Bondora.Service.Interfaces;

namespace Bondora.Controllers
{
    public class HomeController : Controller
    {
        public IEquipmentService EquipmentService;
        public IOrderService OrderService;
        public IMapper MapperConfig;

        public HomeController(IMapper mapperConfig, IEquipmentService equipmentService, IOrderService orderService)
        {
            MapperConfig = mapperConfig;
            EquipmentService = equipmentService;
            OrderService = orderService;
        }

        [OutputCache(Duration = 10, VaryByParam = "none")]
        public ActionResult Index()
        {
            var items = EquipmentService.GetAll().ProjectToList<EquipmentViewModel>(MapperConfig.ConfigurationProvider);
            return View(items);
        }

        [HttpPost]
        public ActionResult Rent(IEnumerable<EquipmentViewModel> model)
        {
            if (!ModelState.IsValid) { return View("Index", model);}

            var rentals = EquipmentService.MakeRentals(model).ToList();

            if (!rentals.Any())
            {
                return RedirectToAction("Index");
            }
            var order = OrderService.MakeOrder(rentals);
            var rentsUrl = Url.Action("Rents");
            if (rentsUrl != null) HttpResponse.RemoveOutputCacheItem(rentsUrl);
            return RedirectToAction("ViewOrder", new {id = order.Id});
        }

        [OutputCache(Duration = 10, VaryByParam = "id")]
        public ActionResult ViewOrder(int id)
        {
            var order = OrderService.GetById(id);
            var orderViewModel = MapperConfig.Map<OrderViewModel>(order);
            return View(orderViewModel);
        }

        [OutputCache(Duration = 10, VaryByParam = "id")]
        public ActionResult GetInvoice(int id)
        {
            var filestring = OrderService.GenerateTextInvoice(id);
            return File(Encoding.UTF8.GetBytes(filestring), "text/plain", $"order-{id}.txt");
        }

        public ActionResult Rents()
        {
            var items = OrderService.GetAll().ToList().Select(itm => MapperConfig.Map<OrderViewModel>(itm));
            return View(items);
        }
    }
}