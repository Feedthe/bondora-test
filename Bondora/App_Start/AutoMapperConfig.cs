﻿using AutoMapper;
using Bondora.Data.Domain;
using Bondora.Data.ViewModels.Home;

namespace Bondora.App_Start
{
    public class AutoMapperConfig
    {
        public static IMapper Config()
        {
            var config = new MapperConfiguration(cfg =>
            {
                EquipmentMapping(cfg);
                RentalMapping(cfg);
                OrderMapping(cfg);
            });

            var map = config.CreateMapper();
            return map;
        }

        private static void OrderMapping(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Order, OrderViewModel>();
        }

        private static void RentalMapping(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Rental, RentalViewModel>()
                .ForMember(member => member.TotalPrice, opt => opt.MapFrom(x => x.CalculatePrice()))
                .ForMember(member => member.TotalBonus, opt => opt.MapFrom(x => x.CalculateBonus()));
        }

        private static void EquipmentMapping(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Equipment, EquipmentViewModel>();
            cfg.CreateMap<EquipmentViewModel, Rental>()
                .ForMember(member => member.Id, opt => opt.Ignore())
                .ForMember(member => member.ItemId, opt => opt.MapFrom(x => x.Id));
        }
    }
}
