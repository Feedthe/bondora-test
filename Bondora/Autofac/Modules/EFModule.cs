﻿using System.Data.Entity;
using Autofac;
using Bondora.Data.Context;
using Bondora.Data.UnitOfWork.Impl;
using Bondora.Data.UnitOfWork.Interfaces;

namespace Bondora.Autofac.Modules
{
    public class EFModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof(BondoraContext)).As(typeof(DbContext)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(UnitOfWork)).As(typeof(IUnitOfWork)).InstancePerLifetimeScope();
        }
    }
}