﻿using System.Reflection;
using Autofac;
using Bondora.Data.Repos.Impl;
using Bondora.Data.Repos.Interfaces;
using Module = Autofac.Module;

namespace Bondora.Autofac.Modules
{
    public class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IRepository<>));

            builder.RegisterAssemblyTypes(Assembly.Load("Bondora.Data"))
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}